package treecutter.taskdata;

import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import treecutter.enums.Tree;

public class TaskDataFactory {
    private TaskDataPackage tree, oak, willow, maple, yew;


     public TaskDataFactory(           TaskDataPackage tree,
                                       TaskDataPackage oak,
                                       TaskDataPackage willow,
                                       TaskDataPackage maple,
                                       TaskDataPackage yew){
        this.tree = tree;
        this.oak = oak;
        this.willow = willow;
        this.maple = maple;
        this.yew = yew;
    }

    public TaskData getTaskData(){
        int woodcutLevel = Skills.getLevel(Skill.WOODCUTTING);

        if(woodcutLevel >= tree.getMinLvl() && woodcutLevel < tree.getMaxLvl()){
            return new TaskData(tree.getLocation(), tree.isBank(), Tree.DEFAULT.getTarget());
        }
        if(woodcutLevel >= oak.getMinLvl() && woodcutLevel < oak.getMaxLvl()){
            return new TaskData(oak.getLocation(), oak.isBank(), Tree.OAK.getTarget());
        }
        if(woodcutLevel >= willow.getMinLvl() && woodcutLevel < willow.getMaxLvl()){
            return new TaskData(willow.getLocation(), willow.isBank(), Tree.WILLOW.getTarget());
        }
        if(woodcutLevel >= maple.getMinLvl() && woodcutLevel < maple.getMaxLvl()){
            return new TaskData(maple.getLocation(), maple.isBank(), Tree.MAPLE.getTarget());

        }if(woodcutLevel >= yew.getMinLvl() && woodcutLevel < yew.getMaxLvl()){
            return new TaskData(yew.getLocation(), yew.isBank(), Tree.YEW.getTarget());
        }

        return null;
    }
}
