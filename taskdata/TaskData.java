package treecutter.taskdata;


import org.rspeer.runetek.api.movement.position.Area;
import treecutter.enums.Location;

public class TaskData {
    private Area chopArea;
    private Area bankArea;
    private boolean bank;
    private String target;
    private String axe = " axe";

    public TaskData(Location location, boolean bank, String target){
        this.chopArea = location.getChopArea();
        this.bankArea = location.getBankArea();
        this.bank = bank;
        this.target = target;

    }

    public void setAxe(String axeString) {
        this.axe = axeString;
    }

    public Area getChopArea() {
        return chopArea;
    }

    public Area getBankArea() {
        return bankArea;
    }

    public boolean isBank() {
        return bank;
    }

    public String getTarget(){
        return this.target;
    }

    public String getAxe() {
        return axe;
    }
}
