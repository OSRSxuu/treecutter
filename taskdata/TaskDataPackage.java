package treecutter.taskdata;

import treecutter.enums.Location;

public class TaskDataPackage {
    private int minLvl;
    private int maxLvl;
    private Location location;
    private boolean bank;


    public TaskDataPackage(int minLvl, int maxLvl, Location location, boolean bank){
        this.minLvl = minLvl;
        this.maxLvl = maxLvl;
        this.location = location;
        this.bank = bank;
    }

    public int getMinLvl() {
        return minLvl;
    }

    public int getMaxLvl() {
        return maxLvl;
    }

    public Location getLocation() {
        return location;
    }

    public boolean isBank() {
        return bank;
    }
}
