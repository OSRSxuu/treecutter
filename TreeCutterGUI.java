package treecutter;
import treecutter.enums.Location;
import treecutter.taskdata.TaskDataFactory;
import treecutter.taskdata.TaskDataPackage;
import treecutter.enums.Tree;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TreeCutterGUI extends JFrame {

    private JLabel      lblHeaderLog,   lblHeaderMinLvl,lblHeaderMaxLvl,    lblHeaderLocation,  lblHeaderBank;
    private JLabel      lblTree,        lblOak,         lblWillow,          lblMaple,           lblYew;
    private JTextField  txtTreeMinLvl,  txtOakMinLvl,   txtWillowMinLvl,    txtMapleMinLvl,     txtYewMinLvl;
    private JTextField  txtTreeMaxLvl,  txtOakMaxLvl,   txtWillowMaxLvl,    txtMapleMaxLvl,     txtYewMaxLvl;
    private JComboBox   cmbTreeLocation, cmbOakLocation, cmbWillowLocation,  cmbMapleLocation,   cmbYewLocation;
    private JCheckBox   ckbTreeBank,    ckbOakBank,     ckbWillowBank,      ckbMapleBank,       ckbYewBank;
    private JButton     btnStart;
    private JLabel      spacer1, spacer2;



    public TreeCutterGUI(){
        super("Xuu's TreeCutter Config");
        setDefaultCloseOperation(HIDE_ON_CLOSE);

        setLayout(new GridLayout(7, 5,20,5));
        //Headers
        lblHeaderLog = new JLabel("Tree Type");
        lblHeaderMinLvl = new JLabel("Min Level");
        lblHeaderMaxLvl = new JLabel("Max Level");
        lblHeaderLocation = new JLabel("Location");
        lblHeaderBank = new JLabel("Bank?");

        //Tree Types
        lblTree = new JLabel("Default");
        lblOak = new JLabel("Oak");
        lblWillow = new JLabel("Willow");
        lblMaple = new JLabel("Maple");
        lblYew = new JLabel("Yew");

        //Min Level
        txtTreeMinLvl = new JTextField("1");
        txtOakMinLvl = new JTextField("15");
        txtWillowMinLvl = new JTextField("30");
        txtMapleMinLvl = new JTextField("0");
        txtYewMinLvl = new JTextField("60");

        //Max Level
        txtTreeMaxLvl = new JTextField("15");
        txtOakMaxLvl = new JTextField("30");
        txtWillowMaxLvl = new JTextField("60");
        txtMapleMaxLvl = new JTextField("0");
        txtYewMaxLvl = new JTextField("99");

        //Location
        cmbTreeLocation = new JComboBox(Tree.DEFAULT.getLocations());
        cmbOakLocation = new JComboBox(Tree.OAK.getLocations());
        cmbWillowLocation = new JComboBox(Tree.WILLOW.getLocations());
        cmbMapleLocation = new JComboBox(Tree.MAPLE.getLocations());
        cmbYewLocation = new JComboBox(Tree.YEW.getLocations());
        //Location Action Listeners
        cmbTreeLocation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                validateCheckBox((Location) cmbTreeLocation.getSelectedItem(),ckbTreeBank);
            }
        });
        cmbOakLocation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                validateCheckBox((Location) cmbOakLocation.getSelectedItem(),ckbOakBank);
            }
        });
        cmbWillowLocation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                validateCheckBox((Location) cmbWillowLocation.getSelectedItem(),ckbWillowBank);
            }
        });
        cmbMapleLocation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                validateCheckBox((Location) cmbMapleLocation.getSelectedItem(),ckbMapleBank);
            }
        });
        cmbYewLocation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                validateCheckBox((Location) cmbYewLocation.getSelectedItem(),ckbYewBank);
            }
        });
        //Bank?
        ckbTreeBank = new JCheckBox();
        ckbOakBank  = new JCheckBox();
        ckbWillowBank  = new JCheckBox();
        ckbMapleBank    = new JCheckBox();
        ckbYewBank = new JCheckBox();

        //Validate checkboxes
        validateCheckBox((Location) cmbTreeLocation.getSelectedItem(),ckbTreeBank);
        validateCheckBox((Location) cmbOakLocation.getSelectedItem(),ckbOakBank);
        validateCheckBox((Location) cmbWillowLocation.getSelectedItem(),ckbWillowBank);
        validateCheckBox((Location) cmbMapleLocation.getSelectedItem(),ckbMapleBank);
        validateCheckBox((Location) cmbYewLocation.getSelectedItem(),ckbYewBank);


        //Button
        btnStart = new JButton("Start!");
        btnStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            TaskDataPackage tree = new TaskDataPackage( Integer.parseInt(txtTreeMinLvl.getText()),
                                                        Integer.parseInt(txtTreeMaxLvl.getText()),
                                                        (Location) cmbTreeLocation.getSelectedItem(),
                                                        ckbTreeBank.isSelected());

            TaskDataPackage oak = new TaskDataPackage(  Integer.parseInt(txtOakMinLvl.getText()),
                                                        Integer.parseInt(txtOakMaxLvl.getText()),
                                                        (Location) cmbOakLocation.getSelectedItem(),
                                                        ckbOakBank.isSelected());

            TaskDataPackage willow = new TaskDataPackage(   Integer.parseInt(txtWillowMinLvl.getText()),
                                                            Integer.parseInt(txtWillowMaxLvl.getText()),
                                                            (Location) cmbWillowLocation.getSelectedItem(),
                                                            ckbWillowBank.isSelected());

            TaskDataPackage maple = new TaskDataPackage(    Integer.parseInt(txtMapleMinLvl.getText()),
                                                            Integer.parseInt(txtMapleMaxLvl.getText()),
                                                            (Location) cmbMapleLocation.getSelectedItem(),
                                                            ckbMapleBank.isSelected());

            TaskDataPackage yew = new TaskDataPackage(  Integer.parseInt(txtYewMinLvl.getText()),
                                                        Integer.parseInt(txtYewMaxLvl.getText()),
                                                        (Location) cmbYewLocation.getSelectedItem(),
                                                        ckbYewBank.isSelected());

            TreeCutter.factory = new TaskDataFactory(tree, oak, willow, maple, yew);
            setVisible(false);
            }
        });
        //Spacers
        spacer1 = new JLabel();
        spacer1.setVisible(false);
        spacer2 = new JLabel();
        spacer2.setVisible(false);

        //row1
        add(lblHeaderLog);
        add(lblHeaderMinLvl);
        add(lblHeaderMaxLvl);
        add(lblHeaderLocation);
        add(lblHeaderBank);

        //row2
        add(lblTree);
        add(txtTreeMinLvl);
        add(txtTreeMaxLvl);
        add(cmbTreeLocation);
        add(ckbTreeBank);

        //row3
        add(lblOak);
        add(txtOakMinLvl);
        add(txtOakMaxLvl);
        add(cmbOakLocation);
        add(ckbOakBank);

        //row4
        add(lblWillow);
        add(txtWillowMinLvl);
        add(txtWillowMaxLvl);
        add(cmbWillowLocation);
        add(ckbWillowBank);

        //row5
        add(lblMaple);
        add(txtMapleMinLvl);
        add(txtMapleMaxLvl);
        add(cmbMapleLocation);
        add(ckbMapleBank);

        //row6
        add(lblYew);
        add(txtYewMinLvl);
        add(txtYewMaxLvl);
        add(cmbYewLocation);
        add(ckbYewBank);

        //row 7
        add(spacer1);
        add(spacer2);
        add(btnStart);




        pack();
    }


    private void validateCheckBox(Location location, JCheckBox checkbox){
        if(location.getBankArea() != null){
            checkbox.setEnabled(true);
            checkbox.setSelected(true);
        }else{
            checkbox.setEnabled(false);
            checkbox.setSelected(false);
        }
    }

}
