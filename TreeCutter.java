package treecutter;

import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.event.listeners.SkillListener;
import org.rspeer.runetek.event.types.SkillEvent;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.task.Task;
import org.rspeer.script.task.TaskScript;
import treecutter.taskdata.TaskData;
import treecutter.taskdata.TaskDataFactory;
import treecutter.tasks.*;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

@ScriptMeta(developer = "Xuu", desc = "Chop wood and bank it", name = "TreeCutter")
public class TreeCutter extends TaskScript implements SkillListener {

    private static final Task[] TASKS = {new Banking(), new Chop(), new Move()};

    public static TaskDataFactory factory;
    public static TaskData td;


    @Override
    public void onStart() {
        TreeCutterGUI gui = new TreeCutterGUI();
        gui.setVisible(true);
        gui.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentHidden(ComponentEvent e) {
                init();
            }
        });

    }

    @Override
    public void onStop() {

    }

    @Override
    public void notify(SkillEvent e) {
        if (e.getType() == SkillEvent.TYPE_LEVEL && e.getSource() == Skill.WOODCUTTING) {
            refreshTaskData();
        }
    }

    private void refreshTaskData() {
        td = factory.getTaskData();
    }

    public void init(){
        refreshTaskData();
        submit(TASKS);
    }
}