package treecutter.enums;

public enum Tree {
    DEFAULT("Tree", Location.LUMBRIDGE),
    OAK("Oak", Location.LUMBRIDGE),
    WILLOW("Willow", Location.DRAYNOR),
    MAPLE("Maple", Location.PLACEHOLDER),
    YEW("Yew", Location.GRAND_EXCHANGE);

    String target;
    Location[] locations;

    Tree(String target, Location... locations){
        this.target = target;
        this.locations = locations;
    }

    public Location[] getLocations(){
        return this.locations;
    }

    public String getTarget(){
        return this.target;
    }


}
