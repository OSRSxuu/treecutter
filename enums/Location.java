package treecutter.enums;

import org.rspeer.runetek.api.movement.position.Area;

public enum Location {
    LUMBRIDGE(  Area.rectangular(3215,3256,3181,3236, 0),
                Area.rectangular(3207,3220,3210,3218, 2)),

    DRAYNOR(    Area.rectangular(3082,3239,3090,3226, 0),
                Area.rectangular(3092,3242,3092,3245,0)),

    GRAND_EXCHANGE(     Area.rectangular(3203,3505,3225,3499, 0),
                        Area.rectangular(3166,3487,3169,3492,0)),
    PLACEHOLDER(null,null);

    Area chopArea;
    Area bankArea;

    Location(Area chopArea, Area bankArea){
        this.chopArea = chopArea;
        this.bankArea = bankArea;
    }

    public Area getChopArea(){
        return this.chopArea;
    }

    public Area getBankArea(){
        return this.bankArea;
    }
}
