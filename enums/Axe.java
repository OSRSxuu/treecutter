package treecutter.enums;

public enum Axe {
    RUNE_AXE("Rune axe", 41),
    ADAMANT_AXE("Adamant axe", 31),
    MITHRIL_AXE("Mithril axe", 21),
    BLACK_AXE("Black axe", 11),
    STEEL_AXE("Steel axe", 6),
    IRON_AXE("Iron axe", 1),
    BRONZE_AXE("Bronze axe", 1),
    MISC_AXE(" axe", 1);

    private String name;
    private int level;

    Axe(String name, int level){
        this.name = name;
        this.level = level;
    }

    public String getName(){
        return name;
    }

    public int getLevel(){
        return level;
    }
}

