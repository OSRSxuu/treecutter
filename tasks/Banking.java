package treecutter.tasks;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;
import treecutter.enums.Axe;

import static treecutter.TreeCutter.td;

public class Banking extends Task {


    @Override
    public boolean validate() {
        return Inventory.isFull() && inBankArea() || !invAxe() && inBankArea();//Checks if inv is full and player is in the banking area

    }

    @Override
    public int execute() {

        if(Inventory.isFull()){
            if(Bank.isOpen()){
                bestAxe();
                Bank.depositAllExcept(x -> x.getName().contains(td.getAxe()));
            }else{
                Bank.open();
            }

        }else if(!invAxe()) {
            if (Bank.isOpen()) {
                bestAxe();
                Bank.withdraw(td.getAxe(), 1);
            } else {
                Bank.open();
            }
        }

        return 300;
    }

    private boolean invAxe(){
        return Inventory.contains(x -> x.getName().contains(td.getAxe()));
    }

    private boolean inBankArea(){
        return td.getBankArea().contains(Players.getLocal());
    }

    private void bestAxe(){
        for(Axe a : Axe.values()){
            if(Bank.contains(a.getName()) &&  Skills.getLevel(Skill.WOODCUTTING) >= a.getLevel()) {
                td.setAxe(a.getName());
                break;
            }
        }
    }
}
