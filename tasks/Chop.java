package treecutter.tasks;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import static treecutter.TreeCutter.td;


public class Chop extends Task {
    private static final String CUT_ACTION = "Chop down";


    @Override
    public boolean validate() {
        return invAxe() && td.getChopArea().contains(Players.getLocal()) && !Inventory.isFull() && !isBusy();
    }

    @Override
    public int execute() {
        Movement.toggleRun(false);
        final SceneObject tree = SceneObjects.getNearest(x -> td.getChopArea().contains(x) && x.getName().equals(td.getTarget()));
        if(tree != null){
            tree.interact(CUT_ACTION);
        }
        return 300;
    }

    private boolean isBusy(){
        return Players.getLocal().isAnimating() || Players.getLocal().isMoving();
    }
    private boolean invAxe(){
        return Inventory.contains(x -> x.getName().contains(td.getAxe()));
    }
}
