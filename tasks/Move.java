package treecutter.tasks;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;
import static treecutter.TreeCutter.td;


public class Move extends Task {
    
    @Override
    public boolean validate() {
        return moveToBank() || moveToWoodCut();
    }

    @Override
    public int execute() {
        if(moveToBank()){
            if(Movement.getRunEnergy() >= 20) Movement.toggleRun(true);
            Movement.walkTo(td.getBankArea().getCenter());
        }
        if(moveToWoodCut()) {
            if(Movement.getRunEnergy() >= 20) Movement.toggleRun(true);
            Movement.walkTo(td.getChopArea().getCenter());
        }
        return 300;
    }

    private boolean moveToBank(){
        return (Inventory.isFull() || !invAxe()) && !td.getBankArea().contains(Players.getLocal()) && !isBusy();
    }

    private boolean moveToWoodCut(){
        return !Inventory.isFull() && !td.getChopArea().contains(Players.getLocal()) && !isBusy() && invAxe();
    }

    private boolean isBusy(){
        return Players.getLocal().isAnimating() || Players.getLocal().isMoving();
    }

    private boolean invAxe(){
        return Inventory.contains(x -> x.getName().contains(td.getAxe()));
    }
}

